# Support pour le cours EDO Sys Dyn MAM3

Ce projet contient le support pédagogique Python pour le cours EDO et systèmes dynamiques pour les MAM3 de Polytech Nice Sophia

Notamment : 
- notebook Jupyter pour l'implémentation du shéma d'Euler explicite 
- notebook Jupyter pour l'étude du système de van der Pol en temps inverse

